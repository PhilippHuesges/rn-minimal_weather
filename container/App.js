import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Location, Permissions } from 'expo';
import Swiper from 'react-native-swiper';

import { API_KEY } from '../utils/keys/OpenWeatherAPI';
import ActualWeather from '../components/actualWeather/actualWeather';
import FutureWeather from '../components/futureWeather/futureWeather';

export default class App extends React.Component {

  lang = 'de';
  state = {
    isLoadingWeather: true,
    isLoadingForecast: true,
    allWeatherData: null,
    allForecastData: null,
    error: null
  };

  componentWillMount() {
    this._getLocationAsyn();
  }

  //get Location Latitude / Longitude if the user granted the location-permission
  _getLocationAsyn = async () => {
    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        error: 'No Permission for Location. Can\'t fetch weather data!'
      });
    }
    else {
      let currentLocation = await Location.getCurrentPositionAsync({ enableHighAccuracy: true });
      await this.fetchForecast(currentLocation.coords.latitude, currentLocation.coords.longitude);
      await this.fetchWeather(currentLocation.coords.latitude, currentLocation.coords.longitude);
    }
  }

  //searching for the right forecastdata for the next 3 days 12 o'clock.
  findCorrectForecastData(json) {

    const tempDate = new Date();
    const tempCurrentDate = new Date(Date.UTC(tempDate.getUTCFullYear(), tempDate.getUTCMonth(), tempDate.getUTCDate()));
    let tempFoundFirstForecast = false;

    json.list.map((tempWeatherData) => {
      if (tempWeatherData.dt_txt.substring(11) === '12:00:00') {
        if (!tempFoundFirstForecast) { //If found data is not from the same day, then we found the data for the next day 12 o'clock.
          let tempForecastFullDate = new Date(tempWeatherData.dt_txt.replace(' ', 'T'));
          let tempForecastDate = new Date(Date.UTC(tempForecastFullDate.getUTCFullYear(), tempForecastFullDate.getUTCMonth(), tempForecastFullDate.getUTCDate()));

          if (tempForecastDate > tempCurrentDate) {
            tempFoundFirstForecast = true;
            this.setState({
              allForecastData: [tempWeatherData],
            });
          }
        }
        else {
          if (this.state.allForecastData.length < 3) {
            this.setState({
              allForecastData: [...this.state.allForecastData, tempWeatherData]
            });
          }
        }
      }
    }
    );

    this.setState({
      isLoadingForecast: false
    });
  };

  //Another Location-function

  /*  componentDidMount() {
     navigator.geolocation.getCurrentPosition(
       position => {
         this.fetchWeather(position.coords.latitude, position.coords.longitude);
         this.fetchForecast(position.coords.latitude, position.coords.longitude);
       },
       error => {
         this.setState({
           error: 'Error Getting Weather Conditions'
         });
       }
     );
   } */

  fetchWeather(latitude, longitude) {
    fetch(
      `http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&APPID=${API_KEY}&units=metric&lang=de`
    )
      .then(res => res.json())
      .then(json => {
        this.setState({
          allWeatherData: json,
          isLoadingWeather: false

        });
      });
  }

  fetchForecast(latitude, longitude) {
    fetch(`http://api.openweathermap.org/data/2.5/forecast?lat=${latitude}&lon=${longitude}&APPID=${API_KEY}&units=metric&lang=de&cnt=28`
    )
      .then(res => res.json())
      .then(json => {
        this.findCorrectForecastData(json);
      })
  }

  render() {
    return (
      <View style={styles.container}>
        {(this.state.isLoadingWeather || this.state.isLoadingForecast) ? (
          <Text>Fetching the Weather</Text>
        ) : (this.state.error !== null) ? (
          <Text>this.state.error</Text>
        ) :
            <Swiper>
              <ActualWeather allWeatherData={this.state.allWeatherData} />
              <FutureWeather allForecastData={this.state.allForecastData} />
            </Swiper>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
