export const allWeatherConditions = {
    Rain: {
        color: '#005bea',
        title: 'Regen',
        subtitle: 'Einfach schwimmen, einfach schwimmen, schwimmen, schwimmen',
        icon: 'weather-rainy'
    },
    Clear: {
        color: '#f7b733',
        title: 'Sonnig',
        subtitle: 'Keine Wolke zu sehen',
        icon: 'weather-sunny'
    },
    Thunderstorm: {
        color: '#616161',
        title: 'Gewitter',
        subtitle: 'Thor könnte es nicht besser machen',
        icon: 'weather-lightning'
    },
    Clouds: {
        color: '#1f1c2c',
        title: 'Bewölkt',
        subtitle: 'Die Sonne spielt verstecken',
        icon: 'weather-cloudy'
    },
    Snow: {
        color: '00d2ff',
        title: 'Schnee',
        subtitle: 'Baut ein Schneepaar',
        icon: 'weather-snowy'
    },
    Drizzle: {
        color: '#076585',
        title: 'Nieselregen',
        subtitle: 'Regenschirm dabei?',
        icon: 'weather-hail'
    },
    Haze: {
        color: '#66a6ff',
        title: 'Dunst',
        subtitle: 'Unangenehm...',
        icon: 'weather-hail'
    },
    Mist: {
        color: '3cd3ad',
        title: 'Nebelig',
        subtitle: 'Er kommt... Dich zu holen!',
        icon: 'weather-fog'
    }
};
    