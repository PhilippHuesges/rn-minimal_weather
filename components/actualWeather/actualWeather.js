import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import { allWeatherConditions } from '../../utils/WeatherConditions';

const actualWeather = ({ allWeatherData }) => {

    const tempWeather = allWeatherData.weather[0].main;
    const tempTemperature = allWeatherData.main.temp;
    return (
        <View style={[
            styles.weatherContainer,
            { backgroundColor: allWeatherConditions[tempWeather].color }
        ]}
        >
            <View style={styles.headerContainer}>
                <MaterialCommunityIcons
                    size={72}
                    name={allWeatherConditions[tempWeather].icon}
                    color={'#fff'} />
                <Text style={styles.temperatureStyle}>
                    {tempTemperature}°
                    </Text>
            </View>
            <View style={styles.midBodyContainer}>
                <Text style={styles.LocationText}>
                    {allWeatherData.name}
                </Text>
            </View>
            <View style={styles.bodyContainer}>
                <Text style={styles.titleText}>
                    {allWeatherConditions[tempWeather].title}
                </Text>
                <Text style={styles.subtitle}>
                    {allWeatherConditions[tempWeather].subtitle}
                </Text>
            </View>
        </View>

    );
};

const styles = StyleSheet.create({
    weatherContainer: {
        flex: 1,
    },
    headerContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    temperatureStyle: {
        fontSize: 72,
        color: '#fff'
    },
    midBodyContainer: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingLeft: 30
    },
    LocationText: {
        fontSize: 72,
        color: '#fff'
    },
    bodyContainer: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingLeft: 25,
        marginBottom: 40
    },
    titleText: {
        fontSize: 60,
        color: '#fff'
    },
    subtitle: {
        fontSize: 24,
        color: '#fff'
    }
});

export default actualWeather;