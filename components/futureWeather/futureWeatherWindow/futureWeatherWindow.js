import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import { allWeatherConditions } from '../../../utils/WeatherConditions';

const futureWeatherWindow = ({ ForecastData }) => {
    return (
        <View style={[
            styles.futureDayContainer,
            { backgroundColor: allWeatherConditions[ForecastData.weather[0].main].color }
        ]}>
            <MaterialCommunityIcons
                style= {styles.titleText}
                name={allWeatherConditions[ForecastData.weather[0].main].icon}
                color={'#fff'} />

            <View style={styles.futureDaySubView}>
                <Text style={styles.subtitle}>
                    {allWeatherConditions[ForecastData.weather[0].main].title}
                </Text>
                <Text style={styles.subtitle}>
                    {ForecastData.main.temp}
                </Text>

            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    futureDayContainer: {
        flex: 1,
    },
    futureDaySubView: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        paddingBottom: 20,
        paddingLeft: 5,
        paddingRight: 5
    },
    titleText: {
        fontSize: 100,
        color: '#fff',
        textAlign: 'center',
        paddingTop: 20
    },
    subtitle: {
        fontSize: 45,
        color: '#fff'
    }
});

export default futureWeatherWindow;