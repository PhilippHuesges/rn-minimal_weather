import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import FutureWeatherWindow from './futureWeatherWindow/futureWeatherWindow'

const futureWeather = ({ allForecastData }) => {


    let tempReturnView = allForecastData.map( (data, index) => {
        return <FutureWeatherWindow 
            ForecastData = {data} 
            key = {index}  /> //should be complete unique, but in this version, index is unique and the data immutable, so index is sufficient
    });

    return (
        <View style={styles.futureContainer}>
            {tempReturnView}
        </View>
    );
};

futureWeather.propTypes = {
    allForecastData: PropTypes.arrayOf(PropTypes.object).isRequired
}

const styles = StyleSheet.create({
    futureContainer: {
        flex: 1,
        justifyContent: 'center',
    }
});

export default futureWeather;